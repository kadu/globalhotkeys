<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.0" language="pl">
<context>
    <name>@default</name>
    <message>
        <source>no buddies to display</source>
        <translation>brak znajomych do wyświetlenia</translation>
    </message>
    <message>
        <source>no accounts defined</source>
        <translation>brak zdefiniowanych kont</translation>
    </message>
    <message>
        <source>Global hotkeys</source>
        <translation>Skróty globalne</translation>
    </message>
    <message>
        <source>Show Kadu&apos;s main window</source>
        <translation>Pokaż główne okno Kadu</translation>
    </message>
    <message>
        <source>Kadu</source>
        <translation>Kadu</translation>
    </message>
    <message>
        <source>Hide Kadu&apos;s main window</source>
        <translation>Ukryj główne okno Kadu</translation>
    </message>
    <message>
        <source>Show/hide Kadu&apos;s main window</source>
        <translation>Pokaż/ukryj główne okno Kadu</translation>
    </message>
    <message>
        <source>Turn silent mode on</source>
        <translation>Włącz tryb cichy</translation>
    </message>
    <message>
        <source>Turn silent mode off</source>
        <translation>Wyłącz tryb cichy</translation>
    </message>
    <message>
        <source>Toggle silent mode</source>
        <translation>Przełącz tryb cichy</translation>
    </message>
    <message>
        <source>Quit Kadu</source>
        <translation>Wyłącz Kadu</translation>
    </message>
    <message>
        <source>Chats</source>
        <translation>Rozmowy</translation>
    </message>
    <message>
        <source>Open incoming chat&apos;s window</source>
        <translation>Otwórz okno rozmowy przychodzącej</translation>
    </message>
    <message>
        <source>Open all incoming chats&apos; windows</source>
        <translation>Otwórz okna wszystkich rozmów przychodzących</translation>
    </message>
    <message>
        <source>Minimize all opened chat windows</source>
        <translation>Zminimalizuj wszystkie otwarte okna rozmów</translation>
    </message>
    <message>
        <source>Restore all minimized chat windows</source>
        <translation>Przywróć wszystkie zminimalizowane okna rozmów</translation>
    </message>
    <message>
        <source>Minimize/restore all chat windows</source>
        <translation>Zminimalizuj/przywróć wszystkie okna rozmów</translation>
    </message>
    <message>
        <source>Close all chat windows</source>
        <translation>Zamknij wszystkie okna rozmów</translation>
    </message>
    <message>
        <source>Open chat with ...</source>
        <translation>Rozpocznij rozmowę z ...</translation>
    </message>
    <message>
        <source>Change status</source>
        <translation>Zmień status</translation>
    </message>
    <message>
        <source>Status</source>
        <translation>Status</translation>
    </message>
    <message>
        <source>Change description</source>
        <translation>Zmień opis</translation>
    </message>
    <message>
        <source>Add a new buddy</source>
        <translation>Dodaj nowego znajomego</translation>
    </message>
    <message>
        <source>Buddies</source>
        <translation>Znajomi</translation>
    </message>
    <message>
        <source>Search for buddy</source>
        <translation>Znajdź znajmego</translation>
    </message>
    <message>
        <source>Windows shortcuts</source>
        <translation>Skróty okien</translation>
    </message>
    <message>
        <source>Configuration window</source>
        <translation>Okno konfiguracji</translation>
    </message>
    <message>
        <source>Account manager window</source>
        <translation>Okno menadżera kont</translation>
    </message>
    <message numerus="yes">
        <source>%n hotkey(s):
- %1
is/are in use by another application.</source>
        <translation>
            <numerusform>%n skrót:
- %1
jest używany przez inną aplikację.</numerusform>
            <numerusform>%n skróty:
- %1
są używane przez inną aplikację.</numerusform>
            <numerusform>%n skrótów:
- %1
jest używanch przez inną aplikację.</numerusform>
        </translation>
    </message>
    <message>
        <source>Shortcut</source>
        <translation>Skrót</translation>
    </message>
    <message>
        <source>Buddies shortcuts</source>
        <translation>Skróty znajomych</translation>
    </message>
    <message>
        <source>Add new shortcut ...</source>
        <translation>Dodaj nowy skrót ...</translation>
    </message>
    <message>
        <source>Buddies (comma separated)</source>
        <translation>Znajomi (rozdzieleni przecinkami)</translation>
    </message>
    <message>
        <source>If possible, show a menu with available chats</source>
        <translation>Jeśli to możliwe, wyświetl menu z dostępnymi opcjami rozmowy</translation>
    </message>
    <message>
        <source>Delete this shortcut</source>
        <translation>Usuń ten skrót</translation>
    </message>
    <message>
        <source>Buddies menus</source>
        <translation>Menu znajomych</translation>
    </message>
    <message>
        <source>Add new menu ...</source>
        <translation>Dodaj nowe menu ...</translation>
    </message>
    <message>
        <source>Shortcuts</source>
        <translation>Skróty</translation>
    </message>
    <message>
        <source>Include current chats</source>
        <translation>Dołącz aktualne rozmowy</translation>
    </message>
    <message>
        <source>Include chats with pending messages</source>
        <translation>Dołącz rozmowy z nieodebranymi wiadomościami</translation>
    </message>
    <message>
        <source>Include recent chats</source>
        <translation>Dołącz ostatnie rozmowy</translation>
    </message>
    <message>
        <source>Include online buddies</source>
        <translation>Dołącz dostępnych znajomych</translation>
    </message>
    <message>
        <source>only from these groups (comma separated)</source>
        <translation>tylko z tych grup (rozdzielone przecinkami)</translation>
    </message>
    <message>
        <source>Treat buddies blocking me as online</source>
        <translation>Traktuj znajomych, którzy mnie blokują jako dostępnych</translation>
    </message>
    <message>
        <source>Include these buddies (comma separated)</source>
        <translation>Dołącz tych znajomych (rozdzieleni przecinkami)</translation>
    </message>
    <message>
        <source>Include buddies from these groups (comma separated)</source>
        <translation>Dołącz znajomych z tych grup (rozdzielone przecinkami)</translation>
    </message>
    <message>
        <source>Exclude these buddies (comma separated)</source>
        <translation>Pomiń tych znajomych (rozdzieleni przecinkami)</translation>
    </message>
    <message>
        <source>Delete this menu</source>
        <translation>Usuń to menu</translation>
    </message>
    <message>
        <source>File transfers window</source>
        <translation>Okno transferu plików</translation>
    </message>
    <message>
        <source>Show at most one item per buddy</source>
        <translation>Pokaż co najwyżej jeden wpis dla jednego znajomego</translation>
    </message>
    <message>
        <source>Sort stateless buddies</source>
        <translation>Sortuj bezstanowych znajomych</translation>
    </message>
    <message>
        <source>Sort by status</source>
        <translation>Uwzględniaj status przy sortowaniu</translation>
    </message>
    <message>
        <source>Multilogon window</source>
        <translation>Okno multilogowania</translation>
    </message>
    <message>
        <source>Always show contact&apos;s identifier</source>
        <translation>Zawsze wyświetlaj identyfikator kontaktu</translation>
    </message>
</context>
</TS>
