<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.0" language="cs">
<context>
    <name>@default</name>
    <message>
        <source>Shortcuts</source>
        <translation>Zkratky</translation>
    </message>
    <message>
        <source>Global hotkeys</source>
        <translation>Celkové zkratky</translation>
    </message>
    <message>
        <source>Buddies shortcuts</source>
        <translation>Zkratky pro kamarády</translation>
    </message>
    <message>
        <source>Buddies menus</source>
        <translation>Nabídka kamarádů</translation>
    </message>
    <message>
        <source>Shortcut</source>
        <translation>Zkratka</translation>
    </message>
    <message>
        <source>Buddies (comma separated)</source>
        <translation>Kamarádi (oddělení čárkou)</translation>
    </message>
    <message>
        <source>If possible, show a menu with available chats</source>
        <translation>Je-li to možné, ukázat nabídku s dostupnými rozhovory</translation>
    </message>
    <message>
        <source>Delete this shortcut</source>
        <translation>Smazat tuto zkratku</translation>
    </message>
    <message>
        <source>Include current chats</source>
        <translation>Zahrnout nynější rozhovory</translation>
    </message>
    <message>
        <source>Include chats with pending messages</source>
        <translation>Zahrnout rozhovory s čekajícími zprávami</translation>
    </message>
    <message>
        <source>Include recent chats</source>
        <translation>Zahrnout nedávné rozhovory</translation>
    </message>
    <message>
        <source>Include online buddies</source>
        <translation>Zahrnout připojené kamarády</translation>
    </message>
    <message>
        <source>only from these groups (comma separated)</source>
        <translation>Jen z těchto skupin (oddělených čárkou)</translation>
    </message>
    <message>
        <source>Treat buddies blocking me as online</source>
        <translation>Postarat se o kamarády, co mě blokují, když jsem připojený</translation>
    </message>
    <message>
        <source>Include these buddies (comma separated)</source>
        <translation>Zahrnout tyto kamarády (oddělené čárkou)</translation>
    </message>
    <message>
        <source>Include buddies from these groups (comma separated)</source>
        <translation>Zahrnout kamarády z těchto skupin (oddělené čárkou)</translation>
    </message>
    <message>
        <source>Exclude these buddies (comma separated)</source>
        <translation>Vyloučit tyto kamarády (oddělené čárkou)</translation>
    </message>
    <message>
        <source>Delete this menu</source>
        <translation>Smazat tuto nabídku</translation>
    </message>
    <message>
        <source>Kadu</source>
        <translation>Kadu</translation>
    </message>
    <message>
        <source>Show Kadu&apos;s main window</source>
        <translation>Ukázat hlavní okno Kadu</translation>
    </message>
    <message>
        <source>Hide Kadu&apos;s main window</source>
        <translation>Skrýt hlavní okno Kadu</translation>
    </message>
    <message>
        <source>Show/hide Kadu&apos;s main window</source>
        <translation>Ukázat/Skrýt hlavní okno Kadu</translation>
    </message>
    <message>
        <source>Turn silent mode on</source>
        <translation>Zapnout tichý režim</translation>
    </message>
    <message>
        <source>Turn silent mode off</source>
        <translation>Vypnout tichý režim</translation>
    </message>
    <message>
        <source>Toggle silent mode</source>
        <translation>Přepnout tichý režim</translation>
    </message>
    <message>
        <source>Quit Kadu</source>
        <translation>Ukončit Kadu</translation>
    </message>
    <message>
        <source>Chats</source>
        <translation>Rozhovory</translation>
    </message>
    <message>
        <source>Open incoming chat&apos;s window</source>
        <translation>Otevřít okno s příchozími rozhovory</translation>
    </message>
    <message>
        <source>Open all incoming chats&apos; windows</source>
        <translation>Otevřít všechna okna s příchozími rozhovory</translation>
    </message>
    <message>
        <source>Minimize all opened chat windows</source>
        <translation>Zmenšit všechna otevřená okna s rozhovory</translation>
    </message>
    <message>
        <source>Restore all minimized chat windows</source>
        <translation>Obnovit všechna zmenšená okna s rozhovory</translation>
    </message>
    <message>
        <source>Minimize/restore all chat windows</source>
        <translation>Zmenšit/Obnovit všechna okna s rozhovory</translation>
    </message>
    <message>
        <source>Close all chat windows</source>
        <translation>Zavřít všechna okna s rozhovory</translation>
    </message>
    <message>
        <source>Open chat with ...</source>
        <translation>Otevřít rozhovor s...</translation>
    </message>
    <message>
        <source>Status</source>
        <translation>Stav</translation>
    </message>
    <message>
        <source>Change status</source>
        <translation>Změnit stav</translation>
    </message>
    <message>
        <source>Change description</source>
        <translation>Změnit popis</translation>
    </message>
    <message>
        <source>Buddies</source>
        <translation>
Kamarádi</translation>
    </message>
    <message>
        <source>Add a new buddy</source>
        <translation>Přidat nového kamaráda</translation>
    </message>
    <message>
        <source>Search for buddy</source>
        <translation>Hledat kamaráda</translation>
    </message>
    <message>
        <source>Windows shortcuts</source>
        <translation>Okenní zkratky</translation>
    </message>
    <message>
        <source>Configuration window</source>
        <translation>Okno s nastavením</translation>
    </message>
    <message>
        <source>Account manager window</source>
        <translation>Okno pro správu účtu</translation>
    </message>
    <message>
        <source>Add new shortcut ...</source>
        <translation>Přidat novou zkratku...</translation>
    </message>
    <message>
        <source>Add new menu ...</source>
        <translation>Přidat novou nabídku...</translation>
    </message>
    <message numerus="yes">
        <source>%n hotkey(s):
- %1
is/are in use by another application.</source>
        <translation>
            <numerusform>%n horká klávesa:
- %1
používaná jiným programem.</numerusform>
            <numerusform>%n horké klávesy:
- %1
používané jinými programy.</numerusform>
            <numerusform>%n horké klávesy:
- %1
používané jinými programy.</numerusform>
        </translation>
    </message>
    <message>
        <source>no buddies to display</source>
        <translation>žádní kamarádi k zobrazení</translation>
    </message>
    <message>
        <source>no accounts defined</source>
        <translation>Nestanoveny žádné účty</translation>
    </message>
    <message>
        <source>File transfers window</source>
        <translation>Okno pro přenosy souborů</translation>
    </message>
    <message>
        <source>Show at most one item per buddy</source>
        <translation>zat alespoň jednu položku na kamaráda</translation>
    </message>
    <message>
        <source>Sort stateless buddies</source>
        <translation>Třídit kamarády bez stavu</translation>
    </message>
    <message>
        <source>Sort by status</source>
        <translation>Třídit podle stavu</translation>
    </message>
    <message>
        <source>Multilogon window</source>
        <translation>Okno s více přihlášeními</translation>
    </message>
    <message>
        <source>Always show contact&apos;s identifier</source>
        <translation type="unfinished"></translation>
    </message>
</context>
</TS>
