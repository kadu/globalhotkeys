<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.0" language="en_US">
<context>
    <name>@default</name>
    <message>
        <source>Shortcuts</source>
        <translation>Shortcuts</translation>
    </message>
    <message>
        <source>Global hotkeys</source>
        <translation>Global hotkeys</translation>
    </message>
    <message>
        <source>Buddies shortcuts</source>
        <translation>Buddies shortcuts</translation>
    </message>
    <message>
        <source>Buddies menus</source>
        <translation>Buddies menus</translation>
    </message>
    <message>
        <source>Shortcut</source>
        <translation>Shortcut</translation>
    </message>
    <message>
        <source>Buddies (comma separated)</source>
        <translation>Buddies (comma separated)</translation>
    </message>
    <message>
        <source>If possible, show a menu with available chats</source>
        <translation>If possible, show a menu with available chats</translation>
    </message>
    <message>
        <source>Delete this shortcut</source>
        <translation>Delete this shortcut</translation>
    </message>
    <message>
        <source>Include current chats</source>
        <translation>Include current chats</translation>
    </message>
    <message>
        <source>Include chats with pending messages</source>
        <translation>Include chats with pending messages</translation>
    </message>
    <message>
        <source>Include recent chats</source>
        <translation>Include recent chats</translation>
    </message>
    <message>
        <source>Include online buddies</source>
        <translation>Include online buddies</translation>
    </message>
    <message>
        <source>only from these groups (comma separated)</source>
        <translation>only from these groups (comma separated)</translation>
    </message>
    <message>
        <source>Treat buddies blocking me as online</source>
        <translation>Treat buddies blocking me as online</translation>
    </message>
    <message>
        <source>Include these buddies (comma separated)</source>
        <translation>Include these buddies (comma separated)</translation>
    </message>
    <message>
        <source>Include buddies from these groups (comma separated)</source>
        <translation>Include buddies from these groups (comma separated)</translation>
    </message>
    <message>
        <source>Exclude these buddies (comma separated)</source>
        <translation>Exclude these buddies (comma separated)</translation>
    </message>
    <message>
        <source>Delete this menu</source>
        <translation>Delete this menu</translation>
    </message>
    <message>
        <source>Kadu</source>
        <translation>Kadu</translation>
    </message>
    <message>
        <source>Show Kadu&apos;s main window</source>
        <translation>Show Kadu&apos;s main window</translation>
    </message>
    <message>
        <source>Hide Kadu&apos;s main window</source>
        <translation>Hide Kadu&apos;s main window</translation>
    </message>
    <message>
        <source>Show/hide Kadu&apos;s main window</source>
        <translation>Show/hide Kadu&apos;s main window</translation>
    </message>
    <message>
        <source>Turn silent mode on</source>
        <translation>Turn silent mode on</translation>
    </message>
    <message>
        <source>Turn silent mode off</source>
        <translation>Turn silent mode off</translation>
    </message>
    <message>
        <source>Toggle silent mode</source>
        <translation>Toggle silent mode</translation>
    </message>
    <message>
        <source>Quit Kadu</source>
        <translation>Quit Kadu</translation>
    </message>
    <message>
        <source>Chats</source>
        <translation>Chats</translation>
    </message>
    <message>
        <source>Open incoming chat&apos;s window</source>
        <translation>Open incoming chat&apos;s window</translation>
    </message>
    <message>
        <source>Open all incoming chats&apos; windows</source>
        <translation>Open all incoming chats&apos; windows</translation>
    </message>
    <message>
        <source>Minimize all opened chat windows</source>
        <translation>Minimize all opened chat windows</translation>
    </message>
    <message>
        <source>Restore all minimized chat windows</source>
        <translation>Restore all minimized chat windows</translation>
    </message>
    <message>
        <source>Minimize/restore all chat windows</source>
        <translation>Minimize/restore all chat windows</translation>
    </message>
    <message>
        <source>Close all chat windows</source>
        <translation>Close all chat windows</translation>
    </message>
    <message>
        <source>Open chat with ...</source>
        <translation>Open chat with ...</translation>
    </message>
    <message>
        <source>Status</source>
        <translation>Status</translation>
    </message>
    <message>
        <source>Change status</source>
        <translation>Change status</translation>
    </message>
    <message>
        <source>Change description</source>
        <translation>Change description</translation>
    </message>
    <message>
        <source>Buddies</source>
        <translation>Buddies</translation>
    </message>
    <message>
        <source>Add a new buddy</source>
        <translation>Add a new buddy</translation>
    </message>
    <message>
        <source>Search for buddy</source>
        <translation>Search for buddy</translation>
    </message>
    <message>
        <source>Windows shortcuts</source>
        <translation>Windows shortcuts</translation>
    </message>
    <message>
        <source>Configuration window</source>
        <translation>Configuration window</translation>
    </message>
    <message>
        <source>Account manager window</source>
        <translation>Account manager window</translation>
    </message>
    <message>
        <source>Add new shortcut ...</source>
        <translation>Add new shortcut ...</translation>
    </message>
    <message>
        <source>Add new menu ...</source>
        <translation>Add new menu ...</translation>
    </message>
    <message numerus="yes">
        <source>%n hotkey(s):
- %1
is/are in use by another application.</source>
        <translation>
            <numerusform>%n hotkey:
- %1
is in use by another application.</numerusform>
            <numerusform>%n hotkeys:
- %1
are in use by another application.</numerusform>
        </translation>
    </message>
    <message>
        <source>no buddies to display</source>
        <translation>no buddies to display</translation>
    </message>
    <message>
        <source>no accounts defined</source>
        <translation>no accounts defined</translation>
    </message>
    <message>
        <source>File transfers window</source>
        <translation>File transfers window</translation>
    </message>
    <message>
        <source>Show at most one item per buddy</source>
        <translation>Show at most one item per buddy</translation>
    </message>
    <message>
        <source>Sort stateless buddies</source>
        <translation>Sort stateless buddies</translation>
    </message>
    <message>
        <source>Sort by status</source>
        <translation>Sort by status</translation>
    </message>
    <message>
        <source>Multilogon window</source>
        <translation>Multilogon window</translation>
    </message>
    <message>
        <source>Always show contact&apos;s identifier</source>
        <translation>Always show contact&apos;s identifier</translation>
    </message>
</context>
</TS>
