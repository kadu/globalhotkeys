<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.0" language="it">
<context>
    <name>@default</name>
    <message>
        <source>Shortcuts</source>
        <translation>Scorciatoie tastiera</translation>
    </message>
    <message>
        <source>Global hotkeys</source>
        <translation>Tasti di scelta rapida globali</translation>
    </message>
    <message>
        <source>Buddies shortcuts</source>
        <translation>Scorciatoie tastiera contatti</translation>
    </message>
    <message>
        <source>Buddies menus</source>
        <translation>Menu contatti</translation>
    </message>
    <message>
        <source>Shortcut</source>
        <translation>Scorciatoia tastiera</translation>
    </message>
    <message>
        <source>Buddies (comma separated)</source>
        <translation>Contatti</translation>
    </message>
    <message>
        <source>If possible, show a menu with available chats</source>
        <translation>Se possibile, mostra il menu con le chat disponibili</translation>
    </message>
    <message>
        <source>Delete this shortcut</source>
        <translation>Cancella questa scorciatoia</translation>
    </message>
    <message>
        <source>Include current chats</source>
        <translation>Includi le chat correnti</translation>
    </message>
    <message>
        <source>Include chats with pending messages</source>
        <translation>Includi le chat con messaggi in sospeso</translation>
    </message>
    <message>
        <source>Include recent chats</source>
        <translation>Includi chat recenti</translation>
    </message>
    <message>
        <source>Include online buddies</source>
        <translation>Includi contatti online</translation>
    </message>
    <message>
        <source>only from these groups (comma separated)</source>
        <translation>solo da questi gruppi</translation>
    </message>
    <message>
        <source>Treat buddies blocking me as online</source>
        <translation>Trattare gli amici che mi hanno bloccato come online</translation>
    </message>
    <message>
        <source>Include these buddies (comma separated)</source>
        <translation>Includi questi contatti</translation>
    </message>
    <message>
        <source>Include buddies from these groups (comma separated)</source>
        <translation>Includi contatti da questi gruppi</translation>
    </message>
    <message>
        <source>Exclude these buddies (comma separated)</source>
        <translation>Escludi questi contatti</translation>
    </message>
    <message>
        <source>Delete this menu</source>
        <translation>Cancella questo menu</translation>
    </message>
    <message>
        <source>Kadu</source>
        <translation>Kadu</translation>
    </message>
    <message>
        <source>Show Kadu&apos;s main window</source>
        <translation>Visualizza il menu principale di Kadu</translation>
    </message>
    <message>
        <source>Hide Kadu&apos;s main window</source>
        <translation>Nascondi il menu principale di Kadu</translation>
    </message>
    <message>
        <source>Show/hide Kadu&apos;s main window</source>
        <translation>Visualizza/nascondi il menu principale di Kadu</translation>
    </message>
    <message>
        <source>Turn silent mode on</source>
        <translation>Accendere modo silenzioso</translation>
    </message>
    <message>
        <source>Turn silent mode off</source>
        <translation>Spegnere modo silenzioso</translation>
    </message>
    <message>
        <source>Toggle silent mode</source>
        <translation>Attivare modo silenzioso</translation>
    </message>
    <message>
        <source>Quit Kadu</source>
        <translation>Abbandona Kadu</translation>
    </message>
    <message>
        <source>Chats</source>
        <translation>Chat</translation>
    </message>
    <message>
        <source>Open incoming chat&apos;s window</source>
        <translation>Apertura finestre di chat in arrivo</translation>
    </message>
    <message>
        <source>Open all incoming chats&apos; windows</source>
        <translation>Apertura di tutte finestre di chat in arrivo</translation>
    </message>
    <message>
        <source>Minimize all opened chat windows</source>
        <translation>Minimizza tutte le finestre di chat aperte</translation>
    </message>
    <message>
        <source>Restore all minimized chat windows</source>
        <translation>Ripristina tutte le finestre di chat aperte</translation>
    </message>
    <message>
        <source>Minimize/restore all chat windows</source>
        <translation>Minimizza/ripristina tutte le finestre di chat aperte</translation>
    </message>
    <message>
        <source>Close all chat windows</source>
        <translation>Chiudi tutte le finestre di chat</translation>
    </message>
    <message>
        <source>Open chat with ...</source>
        <translation>Apri la chat con ...</translation>
    </message>
    <message>
        <source>Status</source>
        <translation>Stato</translation>
    </message>
    <message>
        <source>Change status</source>
        <translation>Cambia stato</translation>
    </message>
    <message>
        <source>Change description</source>
        <translation>Cambia descrizione</translation>
    </message>
    <message>
        <source>Buddies</source>
        <translation>Contatti</translation>
    </message>
    <message>
        <source>Add a new buddy</source>
        <translation>Aggiungi un nuovo contatto</translation>
    </message>
    <message>
        <source>Search for buddy</source>
        <translation>Cerca per contatto</translation>
    </message>
    <message>
        <source>Windows shortcuts</source>
        <translation>Scorciatoie finestre</translation>
    </message>
    <message>
        <source>Configuration window</source>
        <translation>Configurazione finestra</translation>
    </message>
    <message>
        <source>Account manager window</source>
        <translation>Finestra di gestione account</translation>
    </message>
    <message>
        <source>Add new shortcut ...</source>
        <translation>Aggiungi nuova scorciatoia ...</translation>
    </message>
    <message>
        <source>Add new menu ...</source>
        <translation>Aggiungi nuovo menù ...</translation>
    </message>
    <message numerus="yes">
        <source>%n hotkey(s):
- %1
is/are in use by another application.</source>
        <translation>
            <numerusform>%n tasto scelta rapida
%1
è in uso da un&amp;apos;altra applicazione.</numerusform>
            <numerusform>%n tasti di scelta rapida
%1
sono in uso da un&amp;apos;altra applicazione.</numerusform>
        </translation>
    </message>
    <message>
        <source>no buddies to display</source>
        <translation>Nessun contatto da visualizzare</translation>
    </message>
    <message>
        <source>no accounts defined</source>
        <translation>Nessun account definito</translation>
    </message>
    <message>
        <source>File transfers window</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Show at most one item per buddy</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Sort stateless buddies</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Sort by status</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Multilogon window</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Always show contact&apos;s identifier</source>
        <translation type="unfinished"></translation>
    </message>
</context>
</TS>
