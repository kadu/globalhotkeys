#include <X11/keysymdef.h>

#define  GLOBALHOTKEYS_CONFIGVERSION                                              2
#define  GLOBALHOTKEYS_CONFIGURATIONCLEARKEY                           XK_BackSpace
#define  GLOBALHOTKEYS_HOTKEYSTIMERINTERVAL                                     100  /* ms */
#define  GLOBALHOTKEYS_X11SHIFTMASK                                       ShiftMask
#define  GLOBALHOTKEYS_X11CONTROLMASK                                   ControlMask
#define  GLOBALHOTKEYS_X11ALTMASK                                          Mod1Mask
#define  GLOBALHOTKEYS_X11ALTGRMASK                                        Mod5Mask
#define  GLOBALHOTKEYS_X11SUPERMASK                                        Mod4Mask
#define  GLOBALHOTKEYS_X11CAPSLOCKMASK                                     LockMask
#define  GLOBALHOTKEYS_X11NUMLOCKMASK                                      Mod2Mask
#define  GLOBALHOTKEYS_X11LSHIFT                                         XK_Shift_L
#define  GLOBALHOTKEYS_X11RSHIFT                                         XK_Shift_R
#define  GLOBALHOTKEYS_X11LCONTROL                                     XK_Control_L
#define  GLOBALHOTKEYS_X11RCONTROL                                     XK_Control_R
#define  GLOBALHOTKEYS_X11LALT                                             XK_Alt_L
#define  GLOBALHOTKEYS_X11RALT                                             XK_Alt_R
#define  GLOBALHOTKEYS_X11ALTGR                                 XK_ISO_Level3_Shift
#define  GLOBALHOTKEYS_X11LSUPER                                         XK_Super_L
#define  GLOBALHOTKEYS_X11RSUPER                                         XK_Super_R
#define  GLOBALHOTKEYS_GLOBALWIDGETINACTIVITYTIMERINTERVAL                      100  /* ms */
#define  GLOBALHOTKEYS_BUDDIESMENUSMALLICONSIZE                                  16  /* px */
#define  GLOBALHOTKEYS_BUDDIESMENUICONMARGINLEFT                                  9  /* px */
#define  GLOBALHOTKEYS_BUDDIESMENUICONMARGINRIGHT                                 2  /* px */
#define  GLOBALHOTKEYS_BUDDIESMENUICONSPACING                                     9  /* px */
#define  GLOBALHOTKEYS_COMMAREGEXP                                      "\\s*,\\s*"
